package cat712.microblog.repository;

import cat712.microblog.entity.Tag;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Класс для описания репозитория для объектов типа Tag
 * @version 1.0
 */
@Repository
public interface TagRepository extends JpaRepository<Tag, Long>{
    Optional<Tag> findByName(String name);
}
