package cat712.microblog.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cat712.microblog.entity.Person;
import cat712.microblog.repository.PersonRepository;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;
    
    public Optional<Person> findByUsername(String username) {
        return personRepository.findByUsername(username);
    }
}
