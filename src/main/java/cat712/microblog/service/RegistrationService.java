package cat712.microblog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import cat712.microblog.entity.Person;
import cat712.microblog.repository.PersonRepository;
import cat712.microblog.repository.RoleRepository;
import cat712.microblog.security.Role;

@Service
public class RegistrationService {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public void register(Person person) {
        String password = person.getPassword();
        String encodedPassword = passwordEncoder.encode(password);
        person.setPassword(encodedPassword);
        Role role = roleRepository.findByName("USER").get();
        person.setRole(role);
        personRepository.save(person);
    }
}
